import React from 'react';
import ReactLoading from 'react-loading';

const Loader = () => {
    return (
        <div
            className='d-flex justify-content-center align-items-center vh-100'
        >
            <ReactLoading
                color={'#26A65B'}
                type={'bubbles'}
                height={'20%'}
                width={'20%'}
            />


        </div>
    )
}

export default Loader;