import React from 'react';
import {
    BrowserRouter, Route, Switch
} from 'react-router-dom'
import Loader from './components/Loader';

// import our pages / React.lazy - loads our components - allows to run application faster
const Cinemas = React.lazy(() => import('./views/cinemas/Cinemas'));
const Locations = React.lazy(() => import('./views/locations/Locations'));
const Movies = React.lazy(() => import('./views/movies/Movies'));


const MainPage = () => {
    return (
        <BrowserRouter>
            <React.Suspense
                fallback={Loader}
            >
                <Switch>
                    <Route
                        path="/cinemas"
                        render={props => <Cinemas {...props} />}
                    />
                    <Route
                        path="/locations"
                        render={props => <Locations {...props} />}
                    />
                    <Route
                        path="/movies"
                        render={props => <Movies {...props} />}
                    />
                </Switch>
            </React.Suspense>
        </BrowserRouter>
    )
}

export default MainPage;

//BrowserRouter - /cinemas /Movies /locations - strict on website input
//HashRouter - /#/cinemas  /#/movies /#/locations - friendlier on most browsers.