import React, { useEffect, useState } from "react";
import SideBar from "../../components/SideBar";
import { Container, Row, Col, Card, CardHeader, Button, CardBody, Table } from 'reactstrap';
import LocationRow from './LocationRow';
import LocationForm from "./LocationForm";
import Loader from '../../components/Loader';

const Locations = props => {

    const [locations, setLocations] = useState([]);
    const [showForm, setShowForm] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [locationToEdit, setLocationToEdit] = useState({});
    const [idToEdit, setIdToEdit] = useState("");
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {

        setIsLoading(true);

        fetch("http://localhost:4000/admin/locations")
            .then(res => res.json()).then(res => {
                setLocations(res);
                setIsLoading(false);
            })
    }, [locations]);

    const handleSaveLocation = (name, location) => {
        setIsLoading(true);
        let newLocations = [];

        if (isEditing) {
            let editingName = name;
            let editingLocation = location;

            if (name === "") editingName = locationToEdit.name;
            if (location === "") editingLocation = locationToEdit.location;

            fetch('http://localhost:4000/admin/updatelocation', {
                method: "PATCH",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ id: idToEdit, name: editingName, location: editingLocation })
            })
            // .then(res => res.json()).then(res => console.log(res))


            setLocationToEdit({});
            setIdToEdit("");
            setIsEditing(false);

        } else {

            // let apiOptions = {
            //   method: "POST",
            //   headers: { 'Content-Type': 'application/json' },
            //   body: JSON.stringify({
            //     name,
            //     location
            //   })
            // }

            fetch('http://localhost:4000/admin/addlocation', {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ name, location })
            })
        }

        // console.log(newLocations);
        // setLocations(newLocations)
        setShowForm(false);
    };

    const handleDeleteLocation = (id) => {
        const api = {
            method: 'DELETE',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ id })
        }
        fetch("http://localhost:4000/admin/deletelocation", api)
    }

    return (
        <>
            <SideBar />
            <div className="d-flex justify-content-center" style={{ marginLeft: "300px" }}>
                <Container className="mt--7" fluid>
                    <Row>
                        <Card className="col-lg-8 offset-2">
                            <CardHeader className="d-flex flex-column justify-content-center align-items-center">
                                <h1>Locations</h1>
                                <Button
                                    onClick={() => setShowForm(!showForm)}
                                >Add Locations</Button>
                                <LocationForm
                                    showForm={showForm}
                                    setShowForm={setShowForm}
                                    handleSaveLocation={handleSaveLocation}
                                    locationToEdit={locationToEdit}
                                    isEditing={isEditing}
                                    setIsEditing={setIsEditing}
                                />
                            </CardHeader>
                            <CardBody>
                                <Table bordered>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Location</th>
                                            <th
                                                style={{ width: "40%" }}
                                            ></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {locations.map((location, index) => {
                                            return (
                                                <LocationRow
                                                    key={index}
                                                    location={location}
                                                    handleDeleteLocation={() => handleDeleteLocation(location._id)}
                                                    setShowForm={setShowForm}
                                                    setIsEditing={setIsEditing}
                                                    setLocationToEdit={setLocationToEdit}
                                                    setIdToEdit={setIdToEdit}
                                                />
                                            )
                                        })}
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Row>
                </Container>
            </div>
        </>
    )
}

export default Locations;
