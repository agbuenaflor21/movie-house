import React from 'react';
import { Button } from 'reactstrap';

const LocationRow = ({ location, handleDeleteLocation, setShowForm, setIsEditing, setLocationToEdit, setIdToEdit }) => {
    return (
        <tr>
            <td>{location.name}</td>
            <td>{location.location}</td>
            <td>
                <Button color="primary"
                    onClick={() => {
                        setShowForm(true);
                        setIsEditing(true);
                        setLocationToEdit(location);
                        setIdToEdit(location._id);
                    }}
                >Edit</Button>
                <Button color="danger"
                    onClick={handleDeleteLocation}
                >Delete</Button>
            </td>
        </tr>
    )
}

export default LocationRow;